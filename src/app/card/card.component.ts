import { Component, OnInit } from '@angular/core';
import { trigger, state, style, transition, animate, query, animateChild } from '@angular/animations';
import { Word } from '../shared/word';
import { WORDS } from '../shared/words';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  animations: [
    trigger('flipState', [
      state('active', style({
        transform: 'rotateY(179deg)'
      })),
      state('inactive', style({
        transform: 'rotateY(0)'
      })),
      transition('active => inactive',
          animate('300ms ease-out')),
      transition('inactive => active', 
          animate('300ms ease-out'))
    ]),
  ],
})
export class CardComponent implements OnInit {



  words: Word[] = WORDS;
  flip: string = 'inactive';
  show: boolean = false;

  constructor() { }

  ngOnInit(): void {
  }


  toggleFlip() {
    this.flip = (this.flip == 'inactive') ? 'active' : 'inactive';
  }

  toggle() {
    this.show = !this.show;
  }

  nextCard() {

  }

  prevCard() {

  }
}
