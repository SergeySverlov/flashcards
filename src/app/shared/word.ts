export class Word {
    id: number;
    word: string;
    russian: string;
    featured: boolean;
}