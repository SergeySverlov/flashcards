import { Word } from './word';

export const WORDS: Word[] = [
    {
        id: 0,
        word: 'Word',
        russian: 'Мир',
        featured: true
    },
    {
        id: 1,
        word: 'Life',
        russian: 'Жизнь',
        featured: false
    },
    {
        id: 2,
        word: 'Plant',
        russian: 'Растение',
        featured: false
    }
]